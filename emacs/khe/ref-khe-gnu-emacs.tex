\documentclass[8pt, twocolumn]{extarticle}
\usepackage[letterpaper, portrait, textheight=10in, textwidth=8.0in]{geometry}
\usepackage{fancyhdr}  % MUST come after geometry
\usepackage{menukeys}
\usepackage{supertabular}
\usepackage{listings}
\usepackage{lastpage}
\usepackage{tikz}  % for circled numbers
\setlength{\columnsep}{0in}
\usepackage{fbb}   % for \textlbrackdbl and \textrbrackdbl

% \usepackage{kantlipsum}       % for dummy text
% \kant[1-4]                    % generate 4 paragraphs

\pagestyle{fancy}

\begin{document}

\fancyhead{}
\fancyfoot{}
\fancyfoot[L]{The Breviary v0.8.2}
\fancyfoot[C]{\thepage~of~\pageref{LastPage}}
\fancyfoot[R]{Kenneth East <http://east.fm>}
\renewcommand{\headrulewidth}{0pt}

%% \fancyfoot[L]{The Brevarium v0.8.0}
%% \fancyfoot[R]{\thepage}


%% Not surprisingly, all of the lengths/widths defined here interact with each
%% other and with textwidth in the geometry usepackage.  This should be setup
%% in a cleaner way, but at least this is working now.

%% Here's the scheme:  documentation is to be two-column portrait.  We define
%% a single table (which has two columns) and simply add stuff to it.  The
%% table is made with supertable, so supertable decides where to break the
%% table when going from one column to the next.  The single table spans
%% multiple columns across multiple pages.  A mandatory requirement was my not
%% having to decide where column breaks take place.  This is because I expect
%% to modify the document substantially, and I did not want to have to worry
%% about massaging column content each time I decided to modify or
%% re-organize.  My first choice in formats was  three column landscape, but I
%% could never find a way to create a single table, three columns per page,
%% across multiple pages, which automatically calculates columun breaks.  The
%% flexibility I wanted does have its disadvantages -- namely column content
%% isn't as tight as it could be and I end up with breaks in places that I'd
%% prefer not to have them.  But overall, this seems to be a good compromise.

\newlength\TableWidth
\setlength{\TableWidth}{3.5in}  % used for paragraph width in multicol

% \newlength\colA
\newlength\colB
% \setlength{\colA}{.75in}
\setlength{\colB}{3in}


\newcommand{\Key}[1]{\textbf{\texttt{#1}}}
\newcommand{\Func}[1]{\textbf{\texttt{#1}}}

\newcommand\CSHeadingA[1]{\textbf{\LARGE{#1}}}
\newcommand\CSHeadingB[1]{\textbf{\uppercase{#1}}}
\newcommand\CSHeadingC[1]{\textbf{\textsc{#1}}}

\newcommand\CSTableHeading[1]{\multicolumn{2}{l}{#1}\\}
\newcommand\CSTableHeadingA[1]{\hline\\\CSTableHeading{\CSHeadingA{#1}}\\\hline}
\newcommand\CSTableHeadingB[1]{%
  \CSTableContinuation{#1}\\
  \CSTableContinuedFrom{#1}\\
  \CSTableHeading{\CSHeadingB{#1}}\hline
}
\newcommand\CSTableHeadingC[1]{%
  \CSTableHeading{\CSHeadingC{#1}}
}

\newcommand\CSTableRow[2]{#1 & #2\\}

\newcommand\CSTableEmacsCommand[3]{\Key{#1} & \Func{#2} \\ \ & #3 \\}

\newcommand\CSTableContinuation[1]{%
  \tabletail{%
    \hline
    \multicolumn{2}{r}{\small\sl \textbf{#1} continues in the next column\ldots}\\
  }
}

\newcommand\CSTableContinuedFrom[1]{%
  \tablehead{%
    \multicolumn{2}{r}{\small\sl \textbf{#1} continues from the previous column\ldots}\\
    \hline
  }
}

\newcommand*\circled[1]{\tikz[baseline=(char.base)]{
            \node[shape=circle,draw,inner sep=1pt] (char) {#1};}}

\newcommand\CSTableParagraph[1]{\multicolumn{2}{p{\TableWidth}}{#1}\\}

\newcommand\Point[0]{\Func{point}}
\newcommand\Mark[0]{\Func{mark}}
\newcommand\Region{\Func{region}}
\newcommand\Backspace{{\small\keys{backspace}}}
\newcommand\Kring{\Func{killring}}
\newcommand\ModeLine{\Func{mode~line}}
\newcommand\EchoArea{\Func{echo~area}}
\newcommand\Frame{\Func{frame}}
\newcommand\Window{\Func{window}}
\newcommand\Buffer{\Func{buffer}}
\newcommand\MenuBar{\Func{menu bar}}
\newcommand\RegionRect{\Func{region-rectangle}}
\newcommand\KillRect{\Func{kill-rectangle}}
\newcommand\RegularExpression{\emph{regular~expression}}
\newcommand\Regexp{\emph{regexp}}

% Define the keycap symbols that we'll use
%
\newcommand\KEYCAP[1]{{\small\keys{#1}}}
\newcommand\KCRET{\KEYCAP{RET}}
\newcommand\KCDEL{\KEYCAP{DEL}}
\newcommand\KCTAB{\KEYCAP{TAB}}
\newcommand\KCESC{\KEYCAP{ESC}}
\newcommand\KCCTL{\KEYCAP{CTL}}
\newcommand\KCSPC{\KEYCAP{SPC}}
\newcommand\KCMETA{\KEYCAP{META}}

\newcommand\MXB{\textlbrackdbl\Key{M-x}\textrbrackdbl}

\newcommand\PI{\hspace{20pt}}

\tablefirsthead{}
\tablehead{}
\tablelasttail{\hline}
%
\begin{supertabular}{l p{\colB}}

\CSTableHeadingA{GNU Emacs Brevarium}
\CSTableHeadingB{Introduction}
\CSTableParagraph{

\PI On a text-only terminal, Emacs occupies the whole screen.  On windowing
systems, it creates its own windows to use.  The term 
\Frame\ means an entire text-only screen or an entire window used by Emacs.
Emacs usually starts with one frame, but you can add others.

\PI When you start Emacs, the entire \Frame\ except for the first and last
lines is devoted to the text you are editing.  This area is called the \Window.
The first line of the \Frame\ is the \MenuBar\ and the last line is the
\EchoArea.

\PI You can subdivide the large text \Window\ horizontally or vertically into
multiple text windows, each of which can be used for a different file.
\Window\ always refers to the subdivisions of a \Frame.

\PI The \Window\ that the cursor is in is the \emph{selected} \Window, in
which the editing takes place. Most Emacs commands implicitly apply to the
text in the selected \Window.

\PI Each text \Window's last line is a \ModeLine, which displays status
information such as what \Buffer\ is being displayed above it in the \Window,
what major and minor modes are in use, and whether the buffer contains unsaved
changes.

\PI Within Emacs, the cursor shows the location at which editing commands
will take effect.  This location is called the \Point.  You can move
\Point\ with keyboard commands or by clicking the mouse.  While the cursor
appears to point \emph{at} a character, you should think of \Point\ as being
\emph{between} two characters; the \Point\ exists \emph{before} the character
that appears under the cursor and after the character preceding the cursor.

\PI The \Mark\ can be set to remember a location between two characters in the
\Buffer. The \Region\ contains all of the characters between \Point\ and \Mark.

\PI The area at the bottom of the \Frame\ below the \ModeLine\ is the \EchoArea.  It
is used to display small amounts of text for various purposes.

\PI Unlike \Func{vi}, Emacs is a modeless editor.  Emacs functions are
\Func{bound}, or assigned, to keys on the keyboard.  Pressing a key or
key-sequence invokes the function bound to that key or key-sequence.
\Key{C-a} means hold down the \KCCTL\ key \emph{while} pressing the \KEYCAP{a}
key.  The \KCCTL\ key is \emph{always} held down while pressing another key.
On some systems, \KCESC\ functions the same way as \KCCTL, in that it is held
down \emph{while} pressing another key.  On some systems, \KCESC\ must be
pressed and relased, followed by the remaining key(s).  Some systems support
both modes of operation.  The \Key{M} in the sequence \Key{M-f} comes from the
word \KCMETA, which is an older name for \KCESC.

}

\CSTableHeadingB{Entering and Exiting}
\CSTableParagraph{%
  \PI The usual way to invoke Emacs is with the shell command \Func{emacs}.
  If you run Emacs from a shell window, run it in the background with
  \Func{emacs\&}.

  \PI When Emacs starts up, it makes a buffer named \Func{**scratch**}, which
  is the buffer you start out in.  At this point, you will probably want to
  visit one or more files (e.g., via \Key{C-x C-f} as described below).

  The two most common ways to exit Emacs are:
}
\CSTableEmacsCommand{C-z}{suspend-emacs}{If running in a graphical display,
  iconify the current frame.  Otherwise, suspend execution and return to the
  shell prompt; resume with \Key{fg} or \Key{\%emacs}.}
\CSTableEmacsCommand{C-x C-c}{save-buffers-kill-emacs}{Offer to save each
  buffer, then kill this Emacs process.}


\CSTableHeadingB{Invoking Commands}
\CSTableParagraph{%
  \PI Most of the time, you invoke a command via its key binding
  (e.g., Pressing \Key{C-f} invokes the command \Func{forward-char}).  However,
  there will be times when you wish to invoke a command that is not bound to a
  key or for which you do not know its binding.  In those situations, the key
  sequence \Key{M-x} can be used to invoke the desired command by typing the
  command's name.}
\CSTableEmacsCommand{M-x}{execute-extended-command}{%
  \emph{command} \KCRET\ \  [\ \emph{arguments} \KCRET\ ]\newline
  Read \emph{command}, which is the name of an Emacs command,
  then read arguments for \emph{command}, if any, then call
  \emph{command}.}
\CSTableParagraph{%
  Commands in GNU Emacs are implemented as Lisp functions.  In the
  documentation, and elsewhere, you may see \emph{command} and \emph{function}
  used interchangeably.}


\CSTableHeadingB{Command Documentation}
\CSTableParagraph{%
  \PI The bulk of this reference comprises entries describing Emacs commands,
  their key bindings (if any), their additional input (if any), and the
  command's behavior.  Each entry describing a command is a row in a table of
  commands; each entry is organized in the following manner:
}
\CSTableEmacsCommand{\Key{keyseq} \circled{1}}{\Func{command} \circled{2}}{%
  \emph{additional input} \circled{3}\newline
  \emph{description of command} \circled{4}}
\CSTableParagraph{%
  \circled{1} \Key{keyseq}\newline The key sequence on the keyboard that
  \Func{command} is bound to.  Typing the key sequence will invoke
  the command \Func{command}.  If \Key{keyseq} is \MXB\ (\Key{M-x} enclosed
  in double brackets), the the key sequence is \emph{not} bound to
  \Func{command}, rather, the command must be invoked manually by typing
  \Key{M-x}, followed by \Func{command}, followed by \KCRET.}
\CSTableParagraph{%
  \circled{2} \Func{command}\newline The name of the command to invoke.}
\CSTableParagraph{%
  \circled{3} \emph{additional input}\newline If \Func{command} accepts or requires
  additional keyboard input (other than a \emph{prefix argument} - see below),
  that additional input is described here. If \Func{command} does not accept
  additional keyboard input, \emph{additional input} is omitted and
  \emph{description of command} is moved up one line to begin here.}
\CSTableParagraph{%
  \circled{4} \emph{description of command}\newline A description of the
  command's behavior.} 


\CSTableHeadingB{Help}
\CSTableParagraph{%
  \PI GNU Emacs has an extensive help system.  It's worth spending a little time
  with it to understand the resources it offers.  All of the command
  documentation in this document was obtained using \Func{describe-function}.
  }
\CSTableEmacsCommand{C-h b}{describe-bindings}{  Display a buffer showing a list
  of all defined keys, in order of precedence.}
\CSTableEmacsCommand{C-h m}{describe-mode}{display bindings for current major
  mode, the currently enabled minor modes, and the names of all hooks the mode
  calls}
\CSTableEmacsCommand{C-h c}{describe-key-briefly}{\emph{keyseq}\newline
  Prompt for a key sequence, describe its binding in the echo area.}
\CSTableEmacsCommand{C-h f}{describe-function}{\emph{command}\ \KCRET\newline
  Prompt for a command name and and display its description and keyboard
  binding(s), if any.}
\CSTableEmacsCommand{C-h k}{describe-key}{\emph{keyseq}\newline
  Prompt for key sequence, display command name bound to the sequence.}
\CSTableEmacsCommand{\MXB}{apropos}{\emph{pattern}\ \KCRET\newline
  Show all meaningful Lisp symbols whose names match \emph{pattern}.}
\CSTableEmacsCommand{\MXB}{apropos-value}{\emph{pattern}\ \KCRET\newline
  Show all variables containing the specified value.}


\CSTableHeadingB{Editing Text}
\CSTableHeadingC{Inserting Text}
\CSTableParagraph{%
  \PI To insert printing characters into the text you are editing, just type
  them.  Each character that you type is inserted at \Point, moving the cursor
  forward. To delete what you have just inserted, use \keys{DEL}.  \keys{DEL}
  deletes the character before the cursor (actually the character to the left
  of \Point).  To end a line and start a new one, type \keys{RET}.

  \PI Emacs can split lines automatically when they become too long, if you
  turn on a special \emph{minor mode} called \emph{Auto Fill} mode (see
  below). 

  \PI If you prefer to have characters replace (overwrite) existing text
  rather than shove it to the right, you can enable the \emph{Overwrite} minor
  mode.

  \PI Direct insertion works for printing characters and \KCSPC, but other
  characters act as editing commands and do not insert themselves.  If you
  need to insert a control character, use \Func{quoted-insert}.
  }

\CSTableEmacsCommand{\keys{char}}{self-insert-command}{Keys for printed
  characters, such as \Key{a-z, A-Z, 0-9}, punctuation, symbols, etc., are
  bound to \Func{self-insert-command}.  When the key is pressed,
  \Func{self-insert-command} is invoked and the character associated with the
  key is inserted at \Point.  The numeric prefix \Func{N} says how many times
  to repeat the insertion.  For example, pressing the \Key{a} key calls
  \Func{self-insert-command} which inserts the character \Func{a} into the
  buffer at point.}
\CSTableEmacsCommand{C-q}{quoted-insert}{Read the next
  character and insert it.  Useful for inserting control characters.}


\CSTableHeadingC{Changing the Location of Point}
\CSTableEmacsCommand{C-a}{beginning-of-line}{Move point to beginning of line.}
\CSTableEmacsCommand{C-e}{end-of-line}{Move point to end of line.}
\CSTableEmacsCommand{C-f}{forward-char}{Move point one character forward.}
\CSTableEmacsCommand{C-b}{backward-char}{Move point one character backward.}
\CSTableEmacsCommand{M-f}{forward-word}{Move point forward one word.}
\CSTableEmacsCommand{M-b}{backward-word}{Move point backward one word.}
\CSTableEmacsCommand{C-n}{next-line}{Move point down one line.}
\CSTableEmacsCommand{C-p}{previous-line}{Move point up one line.}
\CSTableEmacsCommand{M-r}{move-to-window-line}{Move point to left margin,
  vertically centered in the windows.  Text does not move on the screen.}
\CSTableEmacsCommand{M-<}{beginning-of-buffer}{Move point to the top of the buffer.}
\CSTableEmacsCommand{M->}{end-of-buffer}{Move point to the end of the buffer.}
\CSTableEmacsCommand{M-g c}{goto-char}{\emph{number}\ \KCRET\newline
  Read a number and move point to the \emph{number}th character in the buffer.}
\CSTableEmacsCommand{M-g g}{goto-line}{\emph{number}\ \KCRET\newline
  Read a number and move point to line \emph{number}.}
\CSTableEmacsCommand{C-v}{scroll-up-command}{Scroll text upward by nearly a
  full screen.}
\CSTableEmacsCommand{M-v}{scroll-down-command}{Scroll text downward by nearly
  a full screen.}


\CSTableHeadingC{Erasing Text}
\CSTableEmacsCommand{C-d}{delete-backward-character}{Delete the character
  before \Point.  Usually bound to \keys{DEL} also.}
\CSTableEmacsCommand{C-k}{kill-line}{Kill forward from point to end of line.}
\CSTableEmacsCommand{M-k}{kill-sentence}{Kill forward from point to end of sentence.}
\CSTableEmacsCommand{C-x \KCDEL}{backward-kill-sentence}{Kill backward from point to beginning of sentence.}
\CSTableEmacsCommand{C-M-k}{kill-sexp}{Kill s-expression}
\CSTableEmacsCommand{M-d}{kill-word}{Kill forward from point to end of next word.}
\CSTableEmacsCommand{M-\KCDEL}{backward-kill-word}{Kill backward from point to
  end of next word.} 
\CSTableEmacsCommand{C-w}{kill-region}{Kill text between point and
  mark, saving deleted text in \Kring.  \Key{C-y} can retrieve the text from
  the \Kring.}
\CSTableEmacsCommand{M-w}{kill-ring-save}{Save region as last killed text
  without actually killing it.}
\CSTableEmacsCommand{M-\^}{delete-indentation}{join previous line to current line}
\CSTableEmacsCommand{M-\KCSPC}{just-one-space}{remove all but one space at point}
\CSTableEmacsCommand{M-z}{zap-to-char}{\emph{char}\newline
  Kill up to and including, the \emph{ARG}th occurrence of \emph{char}.}
\CSTableEmacsCommand{\MXB}{zap-up-to-char}{\emph{char}\newline
  Kill up to, but not including, the \emph{ARG}th occurrence of
  \emph{char}. To bind this to \Key{M-z}, see the elisp section.}


\CSTableHeadingC{Undoing Changes}
\CSTableEmacsCommand{C-x u}{undo}{Undo one batch of changes, usually one
  command.}
\CSTableEmacsCommand{C-\_}{undo}{The same. Can be easier if you require
  multiple undos.}
\CSTableEmacsCommand{\MXB}{revert-buffer}{Replace current buffer text with the
  text of the visited from on disk.  Undoes all changes since last visit or
  save.}


\CSTableHeadingC{Files}
\CSTableEmacsCommand{C-x C-f}{find-file}{\emph{filename}\ \KCRET\newline
  Switch to buffer visiting \emph{filename}, creating one if none exits.}
\CSTableEmacsCommand{C-x C-s}{save-buffer}{Save current buffer in visited
  file, if modified.}
\CSTableEmacsCommand{C-x C-v}{find-alternate-file}{\emph{filename}\ \KCRET\newline
  Find \emph{filename}, select its buffer, kill the previous buffer.}
\CSTableEmacsCommand{C-x \KCRET~f}{set-buffer-file-encoding-system}{\emph{encoding}\ \KCRET\newline
  Prompts for desired encoding in mini-buffer then changes buffer's encoding and marks
  buffer as modified. \Key{C-x C-s} will save the buffer.}


\CSTableHeadingC{Blank Lines}
\CSTableEmacsCommand{C-o}{open-line}{Insert one or more blank lines at \Point.}
\CSTableEmacsCommand{C-x C-o}{delete-blank-lines}{Delete all but one of many
  blank lines.}


\CSTableHeadingC{Cursor Position and Other Information}
\CSTableEmacsCommand{\MXB}{what-line}{Print line number at \Point.}
\CSTableEmacsCommand{\MXB}{line-number-mode}{Toggle automatic display of line number.}
\CSTableEmacsCommand{M-=}{count-lines-region}{Print number of lines in current \Region.}
\CSTableEmacsCommand{C-x =}{what-cursor-position}{Print character code of
  character after \Point, character position and column of \Point. Handy to
  identify Unicode characters.}
\CSTableEmacsCommand{\MXB}{describe-char}{Describe the character at point.
  Opens a window containing extensive information about the character.}
\CSTableEmacsCommand{C-x l}{count-lines-page}{display the number of lines in
  the buffer as well as the number of lines before and after mark}


\CSTableHeadingB{Command Arguments}
\CSTableParagraph{
  \PI You can give any Emacs command a \emph{numeric argument}
  (also called a \emph{prefix argument}).  Some commands interpret the
  argument as a repetition count.  For example, \Key{C-f} with an argument of
  10 moves point forward by ten characters instead of one.  With these
  commands, the absence of an argument is equivalent to an argument of 1.

  \PI If your keyboard has a \KCMETA\ key, the easiest way to specify a
  numeric argument is to type \Key{M-1-0 C-f}.

  \PI Another way of specifying an argument is to use the \Key{C-u}
  \Func{universal-argument} command followed by the digits of the argument.
  with \Key{C-u} you can type the argument digits without holding down
  \KCMETA.

  \PI \Key{C-u} followed by a character which is neither a digit nor a minus
  sign has the special meaning of ``multiply by four.''  \Key{C-u} twice
  multiplies it by sixteen, etc.  This is a good way to ``move fast,''  since
  \Key{C-u C-u C-f} moves forward about a fifth of a line.
  \Key{C-u C-u C-o} makes a lot of blank lines.
  \Key{C-u C-k} kills four lines.

  }


\CSTableHeadingB{The Region}
\CSTableEmacsCommand{\MXB}{transient-mark-mode}{Turns on highlighting of
  \Region\ when using a windowing system.  In this mode, a \Region\ ``lasts''
  only temporarily. Handy for visualizing where the \Region\ is.}
\CSTableEmacsCommand{C-\KCSPC}{set-mark-command}{Set the \Mark\ to where
  \Point\ is.}
\CSTableEmacsCommand{C-x C-x}{exchange-point-and-mark}{Interchange \Point\ and
  \Mark.} 
\CSTableEmacsCommand{C-w}{kill-region}{Kill the contents of \Region.}

\CSTableHeadingB{Yanking}
\CSTableParagraph{``Yanking'' means inserting previously killed text. Note
  that text removed by \KCDEL\ (\Func{delete-backward-character}) is
  \emph{deleted} text, not \emph{killed} text, and therefore is can not be
  inserted by yanking.
}
\CSTableEmacsCommand{C-y}{yank}{Yank the last killed text.}
\CSTableEmacsCommand{M-y}{yank-pop}{Replace text just yanked with an earlier
  batch of killed text.}
\CSTableEmacsCommand{C-M-y}{append-next-kill}{Append next kill to last batch
  of killed text.}


\CSTableHeadingB{Searching and Replacing}
\CSTableParagraph{\PI The principal search command is \emph{incremental}; it
  begins to search before you have finished typing the search string.When you
  have typed enough characters to identify the place you want, you can stop.
  You may or may not need to terminate the command with \keys{RET}.  If you
  make a mistake in typing the search string, you can cancel characters with
  \keys{DEL}.  

  \PI Incremental searches generally ignore the case of the text they are
  searching through, \emph{if} you specify the text in lower case.  An
  upper-case letter anywhere in the incremental search string makes the search
  case-sensitive. 

  There are also non-incremental search commands.
}
\CSTableHeadingC{Incremental Search}
\CSTableEmacsCommand{C-s}{isearch-forward}{Incremental search forward.}
\CSTableEmacsCommand{C-r}{isearch-backward}{Incremental search backward.}

\CSTableHeadingC{Non-Incremental Search}
\CSTableEmacsCommand{C-s}{isearch-forward}{\keys{RET}~\emph{string}~\keys{RET}\newline
  Search forward for \emph{string}.}
\CSTableEmacsCommand{C-r}{isearch-forward}{\keys{RET}~\emph{string}~\keys{RET}\newline
  Search backward for \emph{string}.}

\CSTableHeadingC{Regular Expression Search}
\CSTableParagraph{
  \PI A \emph{regular expression} (\emph{regexp} for short) is a
  pattern that denotes a class of alternative strings to match. If you type a
  \KCSPC in incremental regexp search, it matches any sequence of
  whitespace characters, including newlines.  If you want to match just a
  space, type \Key{C-q \KCSPC}.

  \PI Non-incremental regexp search is done by \Func{re-search-forward} and
  \Func{re-search-backward}.  You can invoke these via \Key{M-X}, bind them to
  keys, or invoke them by way of incremental regexp search with
  \Key{C-M-s \keys{RET}} and  \Key{C-M-r \keys{RET}} (see below). 
}
\CSTableEmacsCommand{C-M-s}{isearch-forward-regexp}{\emph{regexp}\newline
  Reads a regexp search string incrementally, searching forward. If no
  \emph{regexp} is provided and \KCRET\ is pressed, prompt for another
  \emph{regexp} which is used in performing a non-incremental regexp search.}
\CSTableEmacsCommand{C-M-r}{isearch-backward-regexp}{\emph{regexp}\newline
  Reads a regexp search string incrementally, searching backward. If no
  \emph{regexp} is provided and \KCRET\ is pressed, prompt for another
  \emph{regexp} which is used in performing a non-incremental regexp search.}


\CSTableHeadingC{Replacement Commands}
\CSTableEmacsCommand{\MXB}{\Key{replace-string}}{%
  \emph{string}~\keys{RET}~\emph{newstring}~\keys{RET}\newline
  Replace every occurrence of \emph{string} with \emph{newstring}.}
\CSTableEmacsCommand{\MXB}{\Key{replace-regexp}}{%
  \emph{regexp}~\keys{RET}~\emph{newstring}~\keys{RET}\newline
  Replace every match for \emph{regexp} with \emph{newstring}.}
\CSTableEmacsCommand{M-\%}{query-replace}{%
  \emph{string}~\keys{RET}~\emph{newstring}~\keys{RET}\newline
  Replace some occurrences of \emph{string} with \emph{newstring}.}

\CSTableHeadingC{Other Search and Loop Commands}
\CSTableEmacsCommand{\MXB}{occur}{%
  \emph{regexp}~\keys{RET}\newline
  Display a list showing each line in buffer that contains a match for
  \emph{regexp}.}

\CSTableHeadingB{Fixing Typos}

\CSTableHeadingC{Killing Mistakes}
\CSTableEmacsCommand{\keys{DEL}}{delete-backward-character}{%
  Delete last character.}
\CSTableEmacsCommand{M-\keys{DEL}}{backward-kill-word}{%
  Kill last word.}
\CSTableEmacsCommand{C-x \keys{DEL}}{backward-kill-sentence}{
  Kill to beginning of sentence.}

\CSTableHeadingC{Transposing Text}
\CSTableEmacsCommand{C-t}{transpose-chars}{Transpose the characters on either
  side of \Point.}
\CSTableEmacsCommand{M-t}{transpose-words}{Transpose two words.}
\CSTableEmacsCommand{C-x C-t}{transpose-lines}{Transpose two lines.}

\CSTableHeadingC{Case Conversion}
\CSTableEmacsCommand{C-x C-l}{downcase-region}{Convert \Region\ to lowercase.}
\CSTableEmacsCommand{C-x C-u}{upcase-region}{Convert \Region\ to uppercase.}

\CSTableHeadingC{Check and Correct Spelling}
\CSTableEmacsCommand{M-\$}{ispell-word}{Check and correct spelling of word at
  \Point.} 
\CSTableEmacsCommand{M-\keys{TAB}}{ispell-complete-word}{Complete the word
  before point using spelling dictionary.}
\CSTableEmacsCommand{\MXB}{ispell-buffer}{Check and correct spelling of buffer.}
\CSTableEmacsCommand{\MXB}{ispell-region}{Check and correct spelling of \Region.}

\CSTableHeadingB{Indenting}
\CSTableEmacsCommand{\KCTAB}{indent-for-tab-command}{%
  Indent the current line or Region, or insert a tab, as appropriate.
  The function called to actually indent the line or insert a tab
  is given by the variable \Func{indent-line-function}.

  \PI If a prefix argument is given, after this function indents the
  current line or inserts a tab, it also rigidly indents the entire
  balanced expression which starts at the beginning of the current
  line, to reflect the current line's indentation.

  \PI If \Func{transient-mark-mode} is turned on and the region is active,
  this function instead calls \Func{indent-region}.  In this case, any
  prefix argument is ignored.
}
\CSTableEmacsCommand{C-M-\textbackslash}{indent-region}{
  Indent each non blank line in the region. A numeric prefix argument specifies
  a column: indent each line to that column.  With no prefix argument, the
  command chooses one of three methods and indents all the lines with it.  See
  the function documentation for more details.
  }
\CSTableEmacsCommand{C-x \KCTAB}{indent-rigidly}{
  Indent all lines starting in \Region. 

  \PI If called interactively with no prefix argument, activate a transient mode
  in which the indentation can be adjusted interactively by typing
  \keys{\arrowkey{<}} and \keys{\arrowkey{>}}. Typing any other key
  deactivates the transient mode.

  \PI If called interactively with prefix argument \Func{N}, indent all lines in
  \Region\ to column \Func{N}.
}
\CSTableEmacsCommand{\MXB}{indent-code-rigidly}{
  Indent all lines of code, starting in the region, sideways by ARG columns.
  Does not affect lines starting inside comments or strings, assuming that
  the start of the region is not inside them.}

\CSTableHeadingB{Filling}
\CSTableEmacsCommand{\MXB}{auto-fill-mode}{Enable or disable Auto Fill mode.
  When enabled, lines are broken automatically at spaces when they get longer
  than the desired width.  The desired width is set in \Func{fill-column}.}
\CSTableEmacsCommand{M-q}{fill-paragraph}{
  Fill paragraph at or after point. If the  Transient Mark mode is enabled and
  the \Mark\ is active, call \Func{fill-region} to fill each of the paragraphs
  in the active region, instead of just filling the current paragraph.
  }
\CSTableEmacsCommand{C-x f}{set-fill-column}{If \Func{ARG} was provided, set
  \Func{fill-column} to \Func{ARG}.  Otherwise, set \Func{fill-column} to the
  current column.}


\CSTableHeadingB{Rectangles}
\CSTableParagraph{The rectangle commands operate on rectangular areas of
  text: all characters between a certain pair of columns, in a certain range
  of lines.  When you specify a rectangle for a command to work on, you do it
  by putting \Mark\ at one corner and \Point\ at opposite corner.  The rectangle
  thus specified is the \RegionRect\ because you control it in the
  same way that you control a \Region.  If \Point\ and \Mark\ are in the same
  column, the \RegionRect\ they define is empty; however, the
  \Region\ they define will be empty only if \Point\ and \Mark\ are at the same
  location. 
}
\CSTableEmacsCommand{\MXB}{clear-rectangle}{Clear the \RegionRect\ by replacing
  its contents with spaces.}
\CSTableEmacsCommand{C-x r k}{kill-rectangle}{Kill the text of the
  \RegionRect, saving its contents as the \KillRect.}
\CSTableEmacsCommand{C-x r d}{kill-rectangle}{Delete the text of the \RegionRect.}
\CSTableEmacsCommand{C-x r y}{yank-rectangle}{Yank the last killed rectangle
  with its upper left corner at \Point.}
\CSTableEmacsCommand{C-x r o}{open-rectangle}{Insert blank space to fill the
  space of \RegionRect.  This pushes the previous contents of the
  \RegionRect\ rightward.}
\CSTableEmacsCommand{\MXB}{string-rectangle
  \keys{RET}~\emph{string}~\keys{RET}}{Insert \emph{string} on each line of
  \RegionRect.}


\CSTableHeadingB{Macros}
\CSTableParagraph{An intro/summary would be helpful here.}
\CSTableEmacsCommand{C-x (}{start-kbd-macro}{}
\CSTableEmacsCommand{C-x (}{end-kbd-macro}{}
\CSTableEmacsCommand{C-x e}{call-last-kbd-macro}{}
\CSTableEmacsCommand{C-/}{undo}{Undo any changes you made during failed
  attempt at macro definition.}
\CSTableEmacsCommand{C-g}{keyboard-quit}{abort macro definition}
\CSTableEmacsCommand{C-x e}{call-last-kbd-macro}{}
\CSTableEmacsCommand{\KCRET}{newline}{Use \KCRET\ to exit incremental search 
  that is embedded within macro}


\CSTableHeadingB{Common to Many Programming Modes}
\CSTableEmacsCommand{M-;}{comment-dwim}{
  Call the appropriate comment command (dwim = Do What I Mean) for the subject
  lines.  If the \Region\ is set, ``comment out'' the lines in \Region, unless
  \Region\ contains only comments, in which case the lines in \Region\ are
  ``uncommented''.  If \Region\ is not set, ``comment out'' or ``uncomment''
  the current line, as appropriate.}


\CSTableParagraph{\vspace*{2.0in}\ }


\CSTableHeadingB{Python Mode - Elpy Minor Mode}
\CSTableParagraph{This describes the python mode in the distribution along with the Elpy
  minor mode.  Note that the desired virtualenv must be active.}
\CSTableHeadingC{General}
\CSTableEmacsCommand{C-c C-d}{elpy-doc}{Display documentation for symbol at \Point.}
\CSTableEmacsCommand{\MXB}{elpy-config}{Test the Elpy configuration, display
  results, and allow customization of the configuration.}
\CSTableHeadingC{Moving \Point\ by Indentation}
\CSTableEmacsCommand{C-down}{elpy-nav-forward-block}{Move to next line with same indent.}
\CSTableEmacsCommand{C-up}{elpy-nav-backward-block}{Move to previous line with same indent.}
\CSTableEmacsCommand{C-left}{elpy-nav-backward-block}{Move to previous indent level or wor.}
\CSTableEmacsCommand{C-right}{elpy-nav-forward-block}{Move to next indent level or word.}
\CSTableHeadingC{Moving the Current Line or Region}
\CSTableEmacsCommand{M-down}{elpy-nav-move-line-or-region-up}{}
\CSTableEmacsCommand{M-up}{elpy-nav-move-line-or-region-down}{}
\CSTableEmacsCommand{M-left}{elpy-nav-move-line-or-region-left}{}
\CSTableEmacsCommand{M-right}{elpy-nav-move-line-or-region-right}{}
\CSTableEmacsCommand{C-c <}{python-indent-shift-left}{
  Shift lines contained in \Region\ by \emph{COUNT} columns to the left.
  \emph{COUNT} defaults to \Func{python-indent-offset}.  If \Region isn't
  active, the current line is shifted.
  }
\CSTableEmacsCommand{C-c >}{python-indent-shift-right}{%
  Shift lines contained in \Region\ by \emph{COUNT} columns to the right.
  \emph{COUNT} defaults to \Func{python-indent-offset}.  If \Region isn't
  active, the current line is shifted.
  }
\CSTableHeadingC{Finding}
\CSTableEmacsCommand{M-.}{xref-find-definitions}{Find definition of identifier at point.}
\CSTableEmacsCommand{C-x 4 M-.}{xref-find-definitions-other-window}{Find definition of identifier at point.}
\CSTableEmacsCommand{C-x 5 M-.}{xref-find-definitions-other-frame}{Find definition of identifier at point.}
\CSTableEmacsCommand{M-,}{xref-pop-marker-stack}{Return to last place where M-. was
  used. Turns M-. and M-, into forward/backward motion for definition lookups.}
\CSTableEmacsCommand{M-?}{xref-find-references}{Find references in buffer for
  identifier.}
\CSTableEmacsCommand{C-M-.}{xref-find-apropos}{Find all meaningful symbols matching
  pattern.}
\CSTableEmacsCommand{C-c C-o}{elpy-occur-definitions}{Search buffer for list of
  definitions of classes and functions.}
\CSTableHeadingC{Completion}
\CSTableEmacsCommand{M-TAB}{elpy-company-backend}{Provide completions at point.  Use
  \Key{M-n}, \Key{M-p}\ to select, \KCRET\ to complete, \KCTAB\ to complete common part.
  Search completions with \Key{C-s}, \Key{C-r}, \Key{C-o}.  \Key{M-0} ... \Key{M-9}  to
  complete using one of first 10 candidates. \Key{F1} displays documentation for
  selected candidate, \Key{C-w} to see its source.}
\CSTableHeadingC{Editing}
\CSTableEmacsCommand{C-c C-e}{multi-edit-symbol-at-point}{}
\CSTableHeadingC{Interactive Python - Send}
\CSTableEmacsCommand{C-c C-c}{elpy-shell-send-region-or-buffer}{}
\CSTableEmacsCommand{C-c C-y c}{elpy-send-defclass}{}
\CSTableEmacsCommand{C-c C-y e}{elpy-send-statement}{}
\CSTableEmacsCommand{C-c C-y f}{elpy-send-defun}{}
\CSTableEmacsCommand{C-c C-y r}{elpy-shell-send-region-or-buffer}{}
\CSTableHeadingC{Interactive Python - Send \& Move \Point}
\CSTableEmacsCommand{C-c C-y C-b}{elpy-shell-send-buffer-and-step}{}
\CSTableEmacsCommand{C-c C-y C-c}{elpy-shell-send-defclass-and-step}{}
\CSTableEmacsCommand{C-c C-y C-e}{elpy-shell-send-statement-and-step}{}
\CSTableEmacsCommand{C-c C-y C-f}{elpy-shell-send-defun-and-step}{}
\CSTableEmacsCommand{C-c C-y C-r}{elpy-shell-send-region-or-buffer-and-step}{}
\CSTableHeadingC{Interactive Python - Send \& Change Focus}
\CSTableEmacsCommand{C-c C-y C-B}{elpy-shell-send-buffer-and-go}{}
\CSTableEmacsCommand{C-c C-y C-C}{elpy-shell-send-defclass-and-go}{}
\CSTableEmacsCommand{C-c C-y C-E}{elpy-shell-send-statement-and-go}{}
\CSTableEmacsCommand{C-c C-y C-F}{elpy-shell-send-defun-and-go}{}
\CSTableEmacsCommand{C-c C-y C-R}{elpy-shell-send-region-or-buffer-and-go}{}


\CSTableHeadingB{ReST Mode}
\CSTableHeadingC{Adornment}
\CSTableEmacsCommand{C-c C-a C-a}{rst-adjust}{
    Cycle through all possible adornments for current content.}
\CSTableEmacsCommand{C-c C-a C-d}{rst-display-adornments-hierarchy}{
    Display current hierarchy of adornments in document.}
\CSTableEmacsCommand{C-c C-a C-s}{rst-straighten-adornments}{
    Homogenize all adornments in document.}
\CSTableHeadingC{Lists}
\CSTableEmacsCommand{C-c C-l C-b}{rst-bullet-list-region}{
   Convert each block of text in region to bullet list.}
\CSTableEmacsCommand{C-c C-l C-e}{rst-enumerate-region}{
   convert each block of text in region to enumerated list}
\CSTableEmacsCommand{C-c C-l C-c}{rst-convert-bullets-to-enumeration}{}
\CSTableEmacsCommand{C-c C-l C-i}{rst-insert-list}{
   insert a list item, prompt for type and optionally number}
\CSTableEmacsCommand{C-c C-l C-s}{rst-straighten-list}{
   Homogenize list to use identical bullet types.}


\CSTableHeadingB{elisp}
\CSTableParagraph{An intro/summary would be helpful here.}
\CSTableEmacsCommand{\MXB}{ielm~\KCRET}{
  Inferior Emacs Lisp Mode - Run an emacs lisp REPL in a buffer.}
\CSTableEmacsCommand{M-:}{eval-expression~\KCRET~\emph{expression}~\KCRET}{
  Read an elisp expression, evaluate it, and display result in echo area.}
\CSTableEmacsCommand{C-x M-:}{repeat-complex-command}{\emph{last-command}~\KCRET\newline
  Edit and re-eval the last complex command, or the ARGth from the last.}
\CSTableEmacsCommand{M-n}{forward-list}{During \Func{eval-expression}, move
  to next expression in history.}
\CSTableEmacsCommand{M-p}{previous-list}{During \Func{eval-expression}, move
  to previous expression in history.}
\CSTableEmacsCommand{C-x C-e}{eval-last-sexp}{Eval the s-expression before
  \Point, display value in echo area.  With prefix argument, place output in
  current buffer.}

\CSTableHeadingB{Shell}
\CSTableParagraph{An intro/summary would be helpful here.}
\CSTableEmacsCommand{M-!}{shell-command\newline\emph{command}~\KCRET}{
  Run a shell command, display its output in a new buffer.}
\CSTableEmacsCommand{M-|}{shell-command-on-region\newline\emph{command}~\KCRET}{
  Run a shell command on region, placing command output in a buffer.}
\CSTableEmacsCommand{C-u M-|}{shell-command-on-region\newline\emph{command}~\KCRET}{
  Run a shell command on region, replacing region with command output.}


\CSTableHeadingB{Useful Unbound Functions}
\CSTableEmacsCommand{-}{list-colors-display}{List all available colors, with
  samples, in a new buffer.}
\CSTableEmacsCommand{-}{customize-themes}{Display and select available
  themes.} 


\CSTableHeadingB{TODO}\\
\CSTableParagraph{
\begin{itemize}
\item Add content to Python Mode, espec. shell interaction.\newline
\item Consider a .emacs section.\newline
\item Cleanup lengths and other defs.\newline
\item Everything from Macros on is just early first draft.  These need work.\newline
\item Interactive shells
\item Reformat command summary to single line (see: ELISP/M-:)
\item Would be nice if column headers were always accurate
\item Lots of extra space a pagebottoms.
\item Useful Unbound Functions is lame.
\item Where command format takes two lines, combine into single line.
\item Commands in each section could bear some reorganization to better
  highlight commonality (e.g., the symmetry between \Key{C-k} and \Key{M-k}.).
\end{itemize}
}
\CSTableEmacsCommand{}{}{}


\end{supertabular}

\bigskip

\CSHeadingB{Emacs Lisp - \emph{elisp}}\\
How to change the default binding for \Key{M-z} to \Func{zap-up-to-char} in
\Func{.emacs}:

\begin{lstlisting}[language=Lisp]
;; Replace zap-to-char with zap-up-to-char
(autoload 'zap-up-to-char "misc"
 "Kill up to, but not including ARGth occurrence of CHAR.

  \(fn arg char)" 'interactive)
(global-set-key "\M-z" 'zap-up-to-char))
\end{lstlisting}

\bigskip

Kenneth East, Austin, Texas  \hfill December 16, 2018

\end{document}


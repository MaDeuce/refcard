#!/usr/bin/env python

import yaml
import os
from pprint import pprint as pp
import pdb
import subprocess
from contextlib import contextmanager

import errno


# The following custom tag handler allows one to create a string
# containing a value from an alias.  By default, YAML does not support 
# this.  !join must be at the beginning of value; it can't be embedded
# within a string.

def join(loader, node):
    seq = loader.construct_sequence(node)
    return ''.join([str(i) for i in seq])
yaml.add_constructor('!join', join)


@contextmanager
def cd(newdir):
    prevdir = os.getcwd()
    os.chdir(os.path.expanduser(newdir))
    try:
        yield
    finally:
        os.chdir(prevdir)


def ensure_path_exists(path):
    try:
        os.makedirs(path)
    except OSError as exception:
        if exception.errno != errno.EEXIST:
            raise


def create_artifact(doc, docdict):
    if 'artifact' not in docdict or 'processing' not in docdict:
        return
    commands = docdict['processing'].split('\n')
    ensure_path_exists('./artifacts')
    with cd('./artifacts'):
        for cmd in commands:
            subprocess.call(cmd, shell=True)
        

class DocList(object):
    '''This object creates a HTML fragment for a single topic.
    The HTML fragments are later assembled into a single HTML 
    page containing reference info for all topic.
    '''

    def __init__(self, package_name):

        self.package_name = package_name
        self._begin()


    def __del__(self):
            
        self._end()


    def _begin(self):
        '''Generates the HTML preamble for a topic.'''

        ensure_path_exists('./artifacts')
        self.index = open('./artifacts/list.html', 'w')

        template = '''
            <!-- This is automatically generated content -- do not edit -->
            <!-- Begin topic -->
            <li class="">
     	        <h3 class="uk-accordion-title">{package}</h3>
   	        <div class="uk-accordion-content">
        '''
        self.index.write(template.format(package=self.package_name))


    def _end(self):
        '''Generates the HTML postamble for a topic.'''

        template = '''
   	            </div>
            </li>
            <!-- End topic -->
        '''
        self.index.write(template)
        self.index.close()

        
    def add_ref(self, descr='', remote='', local=''):
        '''Generates the HTML for a single resource.'''

        if local:
            local = os.path.join(self.package_name, local)
            template = '''
                <!-- Begin resource -->
  	            <div>
      	            <a class="uk-button uk-button-default" href="{remote}">Remote</a>
      	            <a class="uk-button uk-button-default" href="{local}">Local</a>
	                {descr}
	            </div>
                <!-- End resource -->
            '''
        else:
            template = '''
                <!-- Begin resource -->
  	            <div>
      	            <a class="uk-button uk-button-default" href="{remote}">Remote</a>
	                {descr}
	            </div>
                <!-- End resource -->
            '''
        self.index.write(template.format(**locals()))


def main():

    import sys

    # If --nocreate, then assume that artifacts have previously
    # been created, so just create the html.

    try:
        nocreate = sys.argv[1] == '--nocreate'
    except IndexError:
        nocreate = False

    package = os.path.basename(os.getcwd())

    documentation = yaml.load(open('documentation.yml'))

    dl = DocList(package_name=package)
    for doc, docdict in documentation.items():
        if nocreate == False:
            create_artifact(doc, docdict)
        dl.add_ref(descr=docdict['description'],
                   remote=docdict['source'],
                   local=docdict.get('artifact', ''))



if __name__ == '__main__':

    main()


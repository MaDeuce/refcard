PKGS = C emacs git LaTeX make markdown python reSt TeX YAML org-mode yum

.PHONY: $(PKGS) pages

all: packages pages

packages: $(PKGS)

$(PKGS):
	$(MAKE) -C $@

pages: packages
	@./assemble

tidy:
	@echo remove emacs turds
	@find . -name \*~ -exec rm {} \;
	@echo remove wget and unzip numbered backup files
	@find ./*/artifacts/  -iregex '.*[0-9]' -exec rm {} \;

clobber: tidy
	@echo remove artifacts
	@rm -rf */artifacts
	@echo remove pages
	@rm -rf ./pages

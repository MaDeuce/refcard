This is my framework for organizing pointers to reference material that I find
handy and like to have easily accessible.  I don't expect this to be useful to
anyone else, but, if it is, have at it.

Each subdirectory defines a package.  Each package has a documentation.yml
file.  This file defines the location of the reference material.  The material
can be located on the net or locally.  Each package also has a Makefile that
specifies how to process the documentation locally.

For example, the C package specifies the location of a C reference card.  When
`make` is run in the directory, the `processing` section of documentation.yml
specifies how to get a local copy of the card.  During processing, an
`artifact` directory is created for each package.  This directory contains a
local copy of the resource (if so defined in documentation.yml).  It also
contains an HTML fragment with one or more list items, one for each reference
in documentation.yml.

When `make` is run in the top level directory, `make` is invoked on each of
the packages.  When each package has been processed, the information generated
is collected and placed in to the pages directory.  Then, the HTML fragments
for each package are collected and placed in  `pages/index.html`.  This page
is a collapsible tree of links to the collected reference material.  The links
may be local, remote, or both.

Having local links, where possible, ensures that the reference material is
available, even when offline.

The idea is that the pages directory could be published either locally or on
the net so that one can access all of their reference material easily, no
matter where they are.

The `emacs` package has an example of building a refcard from scratch.

To try this out with my chosen references, just clone this repository and run
`make`.  When it finishes, view `pages/index.html`.


This has a lot of warts, but it works fairly well.  I will clean it up more
soon, but it is of such limited interest to anyone other than myself, I don't
intend to do much polishing/refining.



